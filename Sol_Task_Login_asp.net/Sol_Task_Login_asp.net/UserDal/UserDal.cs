﻿using Sol_Task_Login_asp.net.Entity;
using Sol_Task_Login_asp.net.ORD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_Task_Login_asp.net.UserDal
{
    public class UserDal
    {


        #region Declaration 
        private User_LoginDataContext dc = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            dc = new User_LoginDataContext();
        }

        public async Task<bool>User_Validation(User_Info UserInfoObj)
        {
            int? Status = null;
            String message = null;
            return await Task.Run(() =>
            {
                var setQuery =
                dc
                ?.UspUserLogin(
                    User_Validation
                        , UserInfoObj?.User_Id
                        , UserInfoObj?.FirstName
                        , UserInfoObj?.LastName
                         , UserInfoObj?.UserLoginObj?.Email_ID
                        , UserInfoObj?.UserLoginObj?.Password
                        , ref Status
                        , ref message
                        );
                return (Status == 1) ? true : false;
            }

            );
            {

            }
        }

       
    }

}