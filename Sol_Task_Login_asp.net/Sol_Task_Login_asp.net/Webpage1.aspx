﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Webpage1.aspx.cs" Inherits="Sol_Task_Login_asp.net.Webpage1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Css/StyleSheet_login.css" rel="stylesheet" rev="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="TableStyle">

            <table class="InnerStyle">
                <tr>
                    <td>
                        <asp:TextBox ID="txtFirstName" runat="server" placeHolder="Username"></asp:TextBox><br>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:TextBox ID="txtLastName" runat="server" placeHolder="Password"></asp:TextBox><br>
                    </td>
                </tr>

                 <tr>
                    <td>
                        <asp:Button ID="btnSubmit" runat="server" Text="Login" OnClick="btnSubmit_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
