﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Task_Login_asp.net.Entity
{
    public class User_Info
    {
        public int User_Id { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public User_Login UserLoginObj { get; set; }
    }
}